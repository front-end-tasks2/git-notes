import { Navbar } from "../components/navbar/navbar";
import { GlobalStyle } from "./styles";
import { SetCurrentPage } from "../features/set-current-page/set-current-page";

function App() {
  return (
    <div className="App">
      <GlobalStyle />
      <Navbar />
      <SetCurrentPage />
    </div>
  );
}

export default App;
