import { createGlobalStyle } from 'styled-components'

export const GlobalStyle = createGlobalStyle`
  body {
    background-color: white;
    font-size: 16px;
    text-align: center;
    margin-left: 120px;
    margin-right: 120px;
    font-style: helvetica;
    color:#504646;
  }
`;