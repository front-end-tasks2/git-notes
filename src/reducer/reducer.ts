import { createSlice } from "@reduxjs/toolkit";

export enum Screen {
  GistsPage = 1,
  UserProfile,
  ClickedPublicGist,
  ClickedUserGist,
  CreateNewGist,
  YourGists,
  EditGist,
}

const initialState: GistStored = {
  query: "",
  userProfile: {
    login: "",
    name: "",
    avatar_url: "",
    html_url: "",
    logged_in: false,
  },
  publicGists: [],
  currentScreen: Screen.GistsPage,
  clickedGistId: "",
  editGistId: "",
  userGists: [],
};

export type UserProfileType = {
  login: string;
  name: string;
  avatar_url: string;
  html_url: string;
  logged_in: boolean;
};

export interface Gist {
  id: string;
  description: string;
  url: string;
  owner: {
    login: string;
    time: string;
    keyword: string;
    html_url: string;
    avatar_url: string;
  };
  files: {
    filename: string;
    filetype: string;
    type: string;
    raw_url: string;
    file_data: string;
  };
}

export type UserGistType = {
  id: string;
  description: string;
  files: {
    [fileName: string]: {
      raw_url: string;
      content: string;
    };
  };
  created_at: string;
};

export interface GistStored {
  query: string;
  userProfile: UserProfileType;
  publicGists: Gist[];
  currentScreen: Screen;
  clickedGistId: string;
  editGistId: string;
  userGists: UserGistType[];
}

export const gistNotesSlice = createSlice({
  name: "gists",
  initialState,
  reducers: {
    storePublicGists: (state, action) => {
      state.publicGists = action.payload;
    },
    storeQueryString: (state, action) => {
      state.query = action.payload;
    },
    storeGistFile: (state, action) => {
      const i = state.publicGists.findIndex((x) => x.id === action.payload.id);
      state.publicGists[i].files.file_data = action.payload.file_data;
    },
    storeUserProfile: (state, action) => {
      state.userProfile = action.payload;
    },
    setCurrentScreen: (state, action) => {
      state.currentScreen = action.payload;
    },
    setCurrentGist: (state, action) => {
      state.clickedGistId = action.payload;
    },
    storeUserGists: (state, action) => {
      state.userGists = action.payload;
    },
    storeUserGistContent: (state, action) => {
      const index = state.userGists.findIndex(
        (gist) => gist.id === action.payload.id
      );

      state.userGists[index].files[action.payload.filename].content =
        action.payload.content;
    },
    setGistToEdit: (state, action) => {
      state.editGistId = action.payload;
    },
  },
});

export const {
  storePublicGists,
  storeQueryString,
  storeGistFile,
  storeUserProfile,
  setCurrentScreen,
  setCurrentGist,
  storeUserGists,
  storeUserGistContent,
  setGistToEdit,
} = gistNotesSlice.actions;

export default gistNotesSlice.reducer;
