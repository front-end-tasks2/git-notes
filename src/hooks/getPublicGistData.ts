import { Gist } from "../reducer/reducer";

export default function getPublicGistsData(data: any) {
  const gistsData: Gist[] = data.map((gist: any) => ({
    id: gist.id,
    description: gist.description,
    url: gist.html_url,
    owner: {
      id: gist.id,
      login: gist.owner.login,
      html_url: gist.owner.html_url,
      avatar_url: gist.owner.avatar_url,
      time: gist.created_at,
    },
    files: {
      filename: Object.keys(gist.files)[0].split(".")[0],
      filetype: Object.keys(gist.files)[0].split(".")[1],
      type: gist.files[Object.keys(gist.files)[0]].type,
      raw_url: gist.files[Object.keys(gist.files)[0]].raw_url,
      file_data: "",
    },
  }));
  if (gistsData !== undefined && Object.keys(gistsData).length !== 0) {
    return gistsData;
  }
  console.log("Returned undefined: " + data);
  return undefined;
}
