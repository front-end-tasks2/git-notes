import { useEffect, useState } from "react";
import { UserProfileType, storeUserProfile } from "../reducer/reducer";
import { useDispatch } from "react-redux";

const CLIENT_ID = "f1e13fe12f324f549cd9";

type Props = {
  userData: UserProfileType;
  setUserData: (userData: UserProfileType) => void;
};

export function FetchLoginUserData({ userData, setUserData }: Props) {
  const [rerender, setRerender] = useState(false);
  const dispatch = useDispatch();

  console.log(
    "Access token in local storage: " + localStorage.getItem("accessToken")
  );

  useEffect(() => {
    const queryString = window.location.search;
    const urlParams = new URLSearchParams(queryString);
    const codeParams = urlParams.get("code");

    if (codeParams && localStorage.getItem("accessToken") === null) {
      getAccessToken(codeParams, rerender, setRerender);
    }
    if (localStorage.getItem("accessToken")) {
      getUserData();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [localStorage.getItem("accessToken")]);

  async function getUserData() {
    await fetch("http://localhost:4000/getUserData", {
      method: "GET",
      headers: {
        Authorization: "Bearer " + localStorage.getItem("accessToken"),
      },
    })
      .then((response) => {
        return response.json();
      })
      .then((data) => {
        const result: UserProfileType = {
          name: data.name,
          login: data.login,
          avatar_url: data.avatar_url,
          html_url: data.html_url,
          logged_in: true,
        };
        console.log(data);
        setUserData(result);
        dispatch(storeUserProfile(result));
      });
  }
}

async function getAccessToken(
  codeParams: string,
  rerender: boolean,
  setRerender: any
) {
  await fetch("http://localhost:4000/getAccessToken?code=" + codeParams, {
    method: "GET",
  })
    .then((response) => {
      return response.json();
    })
    .then((data) => {
      if (data.access_token) {
        localStorage.setItem("accessToken", data.access_token);
        setRerender(!rerender);
        console.log("Access token:" + data.access_token);
      }
    });
}

export function loginWithGithub() {
  window.location.assign(
    "https://github.com/login/oauth/authorize?client_id=" +
      CLIENT_ID +
      "&scope=user%20gist"
  );
}
