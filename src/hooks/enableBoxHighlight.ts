import { useSelector } from "react-redux";
import { Screen, GistStored } from "../reducer/reducer";

export function EnableBoxHighlight(): boolean {
  const screen: Screen = useSelector(
    (state: GistStored) => state.currentScreen
  );

  return !(
    screen === Screen.ClickedPublicGist || screen === Screen.ClickedUserGist
  );
}
