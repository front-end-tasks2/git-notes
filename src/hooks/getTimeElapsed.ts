import dayjs from "dayjs";
import relativeTime from "dayjs/plugin/relativeTime";

export function getTimeElapsed(time: string) {
  dayjs.extend(relativeTime);
  return dayjs(time).fromNow(true);
}
