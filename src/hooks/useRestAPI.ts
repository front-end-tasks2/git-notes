import useSWR from "swr";
import { GITHUB_API_BASE_URL } from "../constants/githubUrl";

export default function useRestAPI(path: string) {
  console.log("Rest API called with path: " + path);

  const headerItem: { [key: string]: string } = localStorage.getItem(
    "accessToken"
  )
    ? {
        Authorization: "Bearer " + localStorage.getItem("accessToken"),
      }
    : {};

  const fetcher = (url: string) =>
    fetch(url, {
      headers: headerItem,
    }).then((res) => res.json());
  const { data, error, isLoading } = useSWR(
    GITHUB_API_BASE_URL + "/" + path,
    fetcher
  );

  let badCredentials = false;

  if (data?.hasOwnProperty("message") && data?.message === "Bad credentials") {
    badCredentials = true;
    console.log("Bad credentials: " + badCredentials);
    console.log(data);
    console.log("Token: ", localStorage.getItem("accessToken"));
  }

  return {
    badCredentials,
    data,
    isLoading,
    isError: error,
  };
}
