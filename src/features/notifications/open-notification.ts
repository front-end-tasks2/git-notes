import { notification } from "antd";

export const openNotification = (status: string, message: string) => {
    notification.open({
      message: status,
      description: message,
      onClick: () => {
        console.log("Notification Clicked!");
      },
    });
  };
  