import { UserProfilePage } from "../../pages/user-profile/user-profile";
import { useSelector } from "react-redux";
import { GistStored, Screen } from "../../reducer/reducer";
import { PublicGists } from "../../pages/public-gists/public-gists";
import { PublicGistFullView } from "../../pages/public-gist-full-view/gist-full-view";
import { CreateGist } from "../../pages/create-gist/create-gist";
import { EditGist } from "../../pages/edit-gist/edit-gist";
import { UserGistsList } from "../../pages/user-gists/user-gists-list";
import { UserGistFullView } from "../../pages/user-gist-full-view/gist-full-view";

export function SetCurrentPage() {
  const screen: Screen = useSelector(
    (state: GistStored) => state.currentScreen
  );
  return (
    <>{screen === Screen.GistsPage ? <PublicGists /> : <LoadUserProfile />}</>
  );
}

function LoadUserProfile() {
  const screen: Screen = useSelector(
    (state: GistStored) => state.currentScreen
  );

  return (
    <>
      {screen === Screen.UserProfile ? <UserProfilePage /> : <LoadGistPage />}
    </>
  );
}

function LoadGistPage() {
  const screen: Screen = useSelector(
    (state: GistStored) => state.currentScreen
  );

  return (
    <>
      {screen === Screen.ClickedPublicGist ? (
        <PublicGistFullView />
      ) : (
        <LoadCreateGistPage />
      )}
    </>
  );
}

function LoadCreateGistPage() {
  const screen: Screen = useSelector(
    (state: GistStored) => state.currentScreen
  );

  return (
    <>
      {screen === Screen.CreateNewGist ? <CreateGist /> : <LoadEditGistPage />}
    </>
  );
}

function LoadEditGistPage() {
  const screen: Screen = useSelector(
    (state: GistStored) => state.currentScreen
  );

  return <>{screen === Screen.EditGist ? <EditGist /> : <LoadYourGists />}</>;
}

function LoadYourGists() {
  const screen: Screen = useSelector(
    (state: GistStored) => state.currentScreen
  );

  return (
    <>
      {screen === Screen.YourGists ? (
        <UserGistsList preview={true} />
      ) : (
        <LoadClickedUserGist />
      )}
    </>
  );
}

function LoadClickedUserGist() {
  const screen: Screen = useSelector(
    (state: GistStored) => state.currentScreen
  );

  return (
    <>
      {screen === Screen.ClickedUserGist ? (
        <UserGistFullView />
      ) : (
        <PublicGists />
      )}
    </>
  );
}
