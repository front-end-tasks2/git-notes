import { Screen, GistStored, setCurrentScreen } from "../../../reducer/reducer";
import { useSelector, useDispatch } from "react-redux";
import { StyledButton } from "./styles";
import { ArrowLeftOutlined } from "@ant-design/icons";

export function BackButton() {
  const screen: Screen = useSelector(
    (state: GistStored) => state.currentScreen
  );

  const dispatch = useDispatch();

  return (
    <>
      {screen === Screen.GistsPage ? (
        <div style={{marginLeft:"120px"}}></div>
      ) : (
        <>
          <StyledButton
            onClick={() => {
              screen === Screen.EditGist ||
              screen === Screen.CreateNewGist ||
              screen === Screen.ClickedUserGist
                ? dispatch(setCurrentScreen(Screen.YourGists))
                : dispatch(setCurrentScreen(Screen.GistsPage));
            }}
          >
            <ArrowLeftOutlined style={{fontSize: "20px"}}/>
          </StyledButton>
        </>
      )}
    </>
  );
}
