import { StyledGridButton, StyledGridIcon } from "./styles";

type Props = {
  isClicked: boolean;
  onClick: () => void;
};

export function GridButton({ isClicked, onClick }: Props) {
  return (
    <>
      <StyledGridButton clicked={isClicked} onClick={onClick}>
        <StyledGridIcon />
      </StyledGridButton>
    </>
  );
}
