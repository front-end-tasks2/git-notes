import styled from "styled-components";
import { BarsOutlined, AppstoreOutlined } from "@ant-design/icons";

type Props = {
  clicked: boolean;
};

export const StyledButton = styled.button`
  padding: 5px;
  border: none;
  background-color: white;
`;

export const StyledGridButton = styled(StyledButton)`
  color: ${(props: Props) => (props.clicked ? "#04AA6D" : "lightgray")};
`;

export const StyledListButton = styled(StyledButton)`
  color: ${(props: Props) => (props.clicked ? "lightgray" : "#04AA6D")};
`;

export const StyledListIcon = styled(BarsOutlined)`
  font-size: 25px;
`;

export const StyledGridIcon = styled(AppstoreOutlined)`
  font-size: 25px;
`;
