import { StyledListButton,  StyledListIcon} from "./styles";

type Props = {
  isClicked: boolean;
  onClick: () => void;
};

export function ListButton({ isClicked, onClick }: Props) {
  return (
    <>
      <StyledListButton clicked={isClicked} onClick={onClick}>
      <StyledListIcon />
      </StyledListButton>
    </>
  );
}
