import { StyledActionButton } from "../../../pages/user-gists/styles";
import { useState } from "react";
import { ForkOutlined } from "@ant-design/icons";
import { openNotification } from "../../../features/notifications/open-notification";

type Props = {
  id: string;
  preview: boolean;
};

export function ForkButton({ id, preview }: Props) {
  const [isForked, setIsForked] = useState<boolean>(false);

  const handleFork = async () => {
    setIsForked(true);
    try {
      const response = await fetch(
        "https://api.github.com/gists/" + id + "/forks",
        {
          method: "POST",
          headers: {
            Authorization: "Bearer " + localStorage.getItem("accessToken"),
          },
        }
      );

      console.log(response);
      if (response.status >= 200 && response.status < 300) {
        openNotification("Success!", "Gist forked successfully");
      } else {
        openNotification(
          "Error!",
          "Error " + response.status + ". Couldn't fork the gist."
        );
      }
    } catch (error) {
      console.log(error);
      openNotification("Error!", "Couldn't fork the gist.");
    }

    setIsForked(false);
  };

  return (
    <StyledActionButton
      onClick={() => {
        handleFork();
      }}
      clicked={isForked}
    >
      <ForkOutlined />
      &nbsp; {!preview && "Fork"}
    </StyledActionButton>
  );
}
