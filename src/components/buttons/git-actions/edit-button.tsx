import { useDispatch } from "react-redux";
import {
  setGistToEdit,
  UserGistType,
  setCurrentScreen,
  Screen,
} from "../../../reducer/reducer";
import { StyledActionButton } from "../../../pages/user-gists/styles";
import { EditOutlined } from "@ant-design/icons";

type Props = {
  gist: UserGistType;
  preview: boolean;
};

export function EditButton({ gist, preview }: Props) {
  const dispatch = useDispatch();
  return (
    <>
      <StyledActionButton
        onClick={() => {
          dispatch(setGistToEdit(gist.id));
          dispatch(setCurrentScreen(Screen.EditGist));
        }}
      >
        <EditOutlined />
        &nbsp; {!preview && "Edit"}
      </StyledActionButton>
    </>
  );
}
