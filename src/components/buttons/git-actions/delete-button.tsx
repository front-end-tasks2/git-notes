import { StyledActionButton } from "../../../pages/user-gists/styles";
import { DeleteOutlined } from "@ant-design/icons";
import {
  UserGistType,
  GistStored,
  storeUserGists,
  Screen,
  setCurrentScreen,
} from "../../../reducer/reducer";
import { useSelector, useDispatch } from "react-redux";
import { openNotification } from "../../../features/notifications/open-notification";

type Props = {
  gist: UserGistType;
  preview: boolean;
};

export function DeleteButton({ gist, preview }: Props) {
  const dispatch = useDispatch();
  const screen: Screen = useSelector(
    (state: GistStored) => state.currentScreen
  );

  let allGists = useSelector((state: GistStored) => state.userGists);
  const deleteIndex = allGists.findIndex((g) => g.id === gist.id);

  const handleDelete = async () => {
    try {
      const response = await fetch("https://api.github.com/gists/" + gist.id, {
        method: "DELETE",
        headers: {
          Authorization: "Bearer " + localStorage.getItem("accessToken"),
        },
      });

      console.log("Delete request returned: " + response.status);

      if (response.status >= 200 && response.status <= 299) {
        const newGists = [...allGists];
        newGists.splice(deleteIndex, 1);
        dispatch(storeUserGists(newGists));
        openNotification("Ok!", "Gist deleted successfully");

        if (screen === Screen.ClickedUserGist) {
          dispatch(setCurrentScreen(Screen.YourGists));
        }
      } else {
        const msg = "Error " + response.status + ". Couldn't delete gist";
        openNotification("Error", msg);
      }
    } catch (error) {
      console.log(error);
      openNotification("Error", "Could not delete gist.");
    }
  };

  return (
    <>
      <StyledActionButton
        onClick={() => {
          if (window.confirm("Are you sure you wish to delete this gist?"))
            handleDelete();
        }}
      >
        <DeleteOutlined />
        &nbsp; {!preview && "Del"}
      </StyledActionButton>
    </>
  );
}
