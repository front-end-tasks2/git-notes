import { StyledActionButton } from "../../../pages/user-gists/styles";
import { useState } from "react";
import { StarOutlined } from "@ant-design/icons";
import { openNotification } from "../../../features/notifications/open-notification";

type Props = {
  id: string;
  preview: boolean;
};

export function StarButton({ id, preview }: Props) {
  const [isStarred, setIsStarred] = useState<boolean>(false);

  const handleStar = async () => {
    setIsStarred(false);
    try {
      const response = await fetch(
        "https://api.github.com/gists/" + id + "/star",
        {
          method: "PUT",
          headers: {
            Authorization: "Bearer " + localStorage.getItem("accessToken"),
          },
        }
      );

      console.log(response);
      if (response.status >= 200 && response.status < 300) {
        openNotification("Success!", "Gist starred successfully");
      } else {
        openNotification(
          "Error!",
          "Error " + response.status + ". Couldn't star the gist."
        );
      }
    } catch (error) {
      console.log(error);
      openNotification("Error!", "Couldn't star the gist.");
    }

    setIsStarred(true);
  };

  return (
    <StyledActionButton
      onClick={() => {
        handleStar();
      }}
      clicked={isStarred}
    >
      <StarOutlined />
      &nbsp; {!preview && "Star"}
    </StyledActionButton>
  );
}
