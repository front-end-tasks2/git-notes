import logo from "../../../assets/icons/logo.jpg";
import { StyledImage } from "./styles";

export function Logo() {
  return (
    <>
      <StyledImage src={logo} alt="logo" />
    </>
  );
}
