import styled from "styled-components";

export const StyledImage = styled.img`
  margin-top: 20px;
  margin-right: 6px;
  margin-left: 20px;
  width: 150px;
  height: 20px;
`;
