import { StyledLoginBtn, StyledLoggedInDiv, StyledDiv } from "./styles";
import {
  loginWithGithub,
  FetchLoginUserData,
} from "../../../hooks/fetchLoginUserData";
import { UserProfileType, storeUserProfile } from "../../../reducer/reducer";
import { useState } from "react";
import { MenuBar } from "./drop-down-menu/menu-bar";
import { useDispatch } from "react-redux";

export function Login() {
  const [rerender, setRerender] = useState(false);
  const [userData, setUserData] = useState<UserProfileType>({
    login: "",
    name: "",
    avatar_url: "",
    html_url: "",
    logged_in: false,
  });

  const dispatch = useDispatch();

  function onClickLogout() {
    localStorage.removeItem("accessToken");
    setRerender(!rerender);
    const result: UserProfileType = {
      name: "",
      login: "",
      avatar_url: "",
      html_url: "",
      logged_in: false,
    };
    dispatch(storeUserProfile(result));
  }

  FetchLoginUserData({ userData, setUserData });

  return (
    <StyledDiv>
      {localStorage.getItem("accessToken") ? (
        <>
          {Object.keys(userData).length !== 0 ? (
            <StyledLoggedInDiv>
              <MenuBar onClickLogout={onClickLogout} />
            </StyledLoggedInDiv>
          ) : (
            <></>
          )}
        </>
      ) : (
        <>
          <StyledLoginBtn onClick={loginWithGithub}>Login</StyledLoginBtn>
        </>
      )}
    </StyledDiv>
  );
}
