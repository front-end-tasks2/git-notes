import { useSelector, useDispatch } from "react-redux";
import type { MenuProps } from "antd";
import { Dropdown, Space, Typography } from "antd";
import { StyledDownArrow, StyledUserImage, StyledListEntry } from "./styles";
import {
  GistStored,
  setCurrentScreen,
  Screen,
} from "../../../../reducer/reducer";

type Props = {
  onClickLogout: () => void;
};

export function MenuBar({ onClickLogout }: Props) {
  const userProfile = useSelector((state: GistStored) => state.userProfile);
  const dispatch = useDispatch();

  const handleOptionClick = (optionValue: string) => {
    if (optionValue === "Logout") {
      onClickLogout();
    }
  };

  const items: MenuProps["items"] = [
    {
      key: "1",
      label: (
        <>
          <p>Logged in as</p>
          <p>
            <StyledListEntry
              onClick={() => dispatch(setCurrentScreen(Screen.UserProfile))}
            >
            <strong>  {userProfile.name}</strong>
            </StyledListEntry>
          </p>
        </>
      ),
    },
    {
      key: "2",
      label: <hr></hr>,
      disabled: true,
    },

    {
      key: "3",
      label: (
        <StyledListEntry
          onClick={() => dispatch(setCurrentScreen(Screen.YourGists))}
        >
          Your Gists
        </StyledListEntry>
      ),
    },
    {
      key: "4",
      label: (
        <StyledListEntry
          onClick={() => dispatch(setCurrentScreen(Screen.CreateNewGist))}
        >
          Create Gist
        </StyledListEntry>
      ),
    },
    {
      key: "5",
      label: <hr></hr>,
      disabled: true,
    },
    {
      key: "6",
      label: (
        <StyledListEntry
          onClick={() => dispatch(setCurrentScreen(Screen.UserProfile))}
        >
          User Profile
        </StyledListEntry>
      ),
    },
    {
      key: "7",
      label: (
        <StyledListEntry onClick={() => handleOptionClick("Logout")}>
          Logout
        </StyledListEntry>
      ),
    },
  ];

  return (
    <Dropdown
      menu={{
        items,

        defaultSelectedKeys: ["0"],
      }}
    >
      <Typography.Link>
        <Space>
          <StyledUserImage src={userProfile.avatar_url} alt="img" />
          <StyledDownArrow />
        </Space>
      </Typography.Link>
    </Dropdown>
  );
}
