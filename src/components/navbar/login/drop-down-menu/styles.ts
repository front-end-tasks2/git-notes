import styled from "styled-components";
import { DownOutlined } from "@ant-design/icons";

export const StyledDownArrow = styled(DownOutlined)`
  color: white;
`;

export const StyledList = styled.ul`
  list-style-type: none;
  border: solid 1px gray;
  font-size: 12px;
  margin-left: -100px;
  padding-top: 10px;
  padding-botton: 30px;
  background-color: lightgray;
  width: 100px;
  position: absolute;
  text-align: left;
`;

export const StyledListEntry = styled.button`
  padding-top: 10px;
  padding-bottom: 10px;
  background-color: white;
  border: none;
`;

export const StyledListEntryURL = styled.a`
  color: #04aa6d;
  text-decoration: none;
  padding-top: 10px;
  padding-bottom: 10px;
  font-weight: bold;
`;

export const StyledDropDownButton = styled.button`
  margin-top: 15px;
  width: 3px;
  color: white;
  background-color: #04aa6d;
  border: none;
  margin-left: 0;
  padding-left: 0;
`;

export const StyledUserImage = styled.img`
  height: 40px;
  width: 40px;
  border-radius: 50%;
  border: solid 1px white;
  background-color: white;
  margin-top: -5px;
`;
