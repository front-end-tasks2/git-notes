import styled from "styled-components";

export const StyledLoginBtn = styled.button`
  color: #04aa6d;
  background-color: white;
  border: none;
  border-radius: 5px;
  height: 30px;
  width: 80px;
  margin-top: 3px;
  font-size:14px;
`;

export const StyledUserImage = styled.img`
  height: 30px;
  width: 30px;
  border-radius: 50%;
  border: solid 1px white;
  background-color: white;
  margin-top: 5px;
`;

export const StyledLoggedInDiv = styled.div`
  display: flex;
  flex-direction: row;
`;

export const StyledDiv = styled.div`
  margin-right: 120px;
`;
