import styled from "styled-components";
import { SearchOutlined } from "@ant-design/icons";

export const StyledSearchButton = styled.div`
  color: white;
  background-color: #04aa6d;
  border: none;
  size: 16px;
`;

export const StyledSearchInput = styled.input`
  font-size: 14px;
  height: 30px;
  width: 300px;
  border-radius: 5px;
  background-color: #04aa6d;
  color: white;
  border: solid 1px white;
`;

export const StyledSearchIcon = styled(SearchOutlined)`
  color: white;
  background-color: none;
  border: none;
  margin-top: 10px;
  font-size: 16px;
`;
