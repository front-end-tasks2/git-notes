import { StyledSearchInput, StyledSearchIcon } from "./styles";
import { Col, Row } from "antd";
import { useDispatch } from "react-redux";
import { storeQueryString } from "../../../reducer/reducer";
import "./styles.css";

export function SearchBar() {
  const dispatch = useDispatch();

  const handleSearch = (e: React.ChangeEvent<HTMLInputElement>) => {
    dispatch(storeQueryString(e.target.value));
  };

  return (
    <Row>
      <Col span={20}>
        <StyledSearchInput
          type="text"
          onChange={handleSearch}
          placeholder="Search Notes..."
        />
      </Col>
      <Col span={3}>
        <StyledSearchIcon />
      </Col>
    </Row>
  );
}
