import { StyledNavBar, StyledDiv } from "./styles";
import { SearchBar } from "./search-bar/search-bar";
import { Login } from "./login/login";
import { Logo } from "./logo/logo";
import { BackButton } from "../buttons/back-button/back-button";

export function Navbar() {
  return (
    <StyledNavBar>
      <BackButton />
      <Logo />
      <StyledDiv>
        <SearchBar />
        <Login />
      </StyledDiv>
    </StyledNavBar>
  );
}
