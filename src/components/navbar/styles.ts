import styled from "styled-components";
import { Row } from "antd";

export const StyledNavBar = styled(Row)`
  background-color: #04aa6d;
  margin: 10px -120px 20px ;
  height:60px;
`;

export const StyledDiv = styled.div`
  display: flex;
  flex-direction: row;
  margin: 12px 0 0 auto;
`;