import { useForm } from "react-hook-form";
import { StyledTextBox, StyledInput } from "./styles";

export interface IFormInput {
  pageNumber: number;
}

type Props = {
  onSubmit: (arg0: IFormInput) => void;
};

export function JumpToPage({ onSubmit }: Props) {
  const { register, handleSubmit } = useForm<IFormInput>();

  return (
    <StyledInput>
      <div style={{marginTop:"16px", marginRight:"15px "}}>Page</div>
      <form onSubmit={handleSubmit(onSubmit)}>
        <StyledTextBox
          type="number"
          {...register("pageNumber", {
            valueAsNumber: true,
            max: 100,
            min: 1,
          })}
        />
      </form>
     <div style={{marginTop:"16px", marginLeft:"15px "}}>of 100</div>
    </StyledInput>
  );
}
