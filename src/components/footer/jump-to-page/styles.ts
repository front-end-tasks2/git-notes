import styled from "styled-components";

export const StyledInput = styled.div`
  display: flex;
  flex-direction: row;
  color: #04aa6d;
  font-size: 12px;
  font-weight: bold;
`;

export const StyledTextBox = styled.input`

  color: #04aa6d;
  border: solid 1px #04AA6D;
  border-radius:5px;
  width: 50px;
  height: 30px;
  font-size: 12px;
  font-weight: bold;
  margin-top:4px;
  align-items:center;
`;
