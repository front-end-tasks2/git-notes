import { useState } from "react";
import { StyledFooterButtons } from "./styles";
import { JumpToPage, IFormInput } from "./jump-to-page/jump-to-page";
import { useDispatch } from "react-redux";
import { Col } from "antd";
import { NextPage } from "./next-prev-page-buttons/next-page";
import { PrevPage } from "./next-prev-page-buttons/prev-page";
import useRestAPI from "../../hooks/useRestAPI";
import getPublicGistsData from "../../hooks/getPublicGistData";
import { storePublicGists } from "../../reducer/reducer";
import { StyledButtonsDiv } from "../../layouts/list/styles";

export const Footer = () => {
  const [currentPage, setCurrentPage] = useState<number>(1);
  const dispatch = useDispatch();

  const { data, isLoading, isError } = useRestAPI(
    `gists/public?page=${currentPage}`
  );

  if (currentPage > 1) {
    if (isLoading) {
      console.log("Loading");
    } else if (data) {
      const publicGistData = getPublicGistsData(data);
      dispatch(storePublicGists(publicGistData));
    } else if (isError) {
      console.log("Error");
    }
  }

  const onSubmit = (data: IFormInput) => {
    console.log(data.pageNumber);
    setCurrentPage((currentPage) => data.pageNumber);
  };

  return (
    <div>
      <footer>
        <StyledFooterButtons>
          <Col span={1}></Col>

          <Col span={6} offset={8}>
            <StyledButtonsDiv>
              <PrevPage
                isDisabled={currentPage <= 1}
                onClick={() => {
                  setCurrentPage(currentPage - 1);
                }}
              />
              <NextPage
                isDisabled={currentPage >= 100}
                onClick={() => {
                  setCurrentPage(currentPage + 1);
                }}
              />{" "}
            </StyledButtonsDiv>
          </Col>

          <Col span={4} offset={5}>
            <JumpToPage onSubmit={onSubmit} />
          </Col>
        </StyledFooterButtons>
      </footer>
    </div>
  );
};
