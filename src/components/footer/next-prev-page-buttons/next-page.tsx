import { StyledButton } from "./styles";
import { ArrowRightOutlined } from "@ant-design/icons";

type Props = {
  isDisabled: boolean;
  onClick: () => void;
};

export function NextPage({ isDisabled, onClick }: Props) {
  return (
    <StyledButton disabled={isDisabled} onClick={onClick}>
      Next Page <ArrowRightOutlined />
    </StyledButton>
  );
}
