import styled from "styled-components";

type Props = {
  disabled?: boolean;
};

export const StyledButton = styled.button`
  color: white;
  border: none;
  border-radius: 5%;
  content-align: center;
  height: 40px;
  width:120px;
  font-size: 12px;
  font-weight: bold;
  background-color: ${(props: Props) => (props.disabled ? "gray" : "#04AA6D")};
  margin:0 5px 0 5px;
`;
