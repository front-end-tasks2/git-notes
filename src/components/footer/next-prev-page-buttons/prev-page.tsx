import { StyledButton } from "./styles";
import { ArrowLeftOutlined } from "@ant-design/icons";

type Props = {
  isDisabled: boolean;
  onClick: () => void;
};

export function PrevPage({ isDisabled, onClick }: Props) {
  return (
    <StyledButton disabled={isDisabled} onClick={onClick}>
      Prev Page <ArrowLeftOutlined />
    </StyledButton>
  );
}
