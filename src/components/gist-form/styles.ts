import styled from "styled-components";
import { Form } from "antd";
import { DeleteOutlined } from "@ant-design/icons";

type Props = {
  disabled?: boolean;
};

export const StyledForm = styled(Form)`
  margin-top: 100px;
`;

export const StyledButton = styled.button`
  color: white;
  border: none;
  border-radius: 10%;
  width: 90px;
  height: 30px;
  font-weight: bold;
  background-color: ${(props: Props) =>
    props.disabled ? "lightgray" : "#04AA6D"};
`;

export const StyledFormItem = styled(Form.Item)`
  width: 100%;
  margin-left: 20%;
`;

export const StyledDeleteButton = styled(DeleteOutlined)`
  color: red;
  font-size: 20px;
  margin: 3px 0 0 3px;
  border: solid 1px lightgray;
  padding: 3px;
  border-radius: 5px;
`;
