import { Form, Input, Alert, Col, Row } from "antd";
import {
  StyledForm,
  StyledButton,
  StyledFormItem,
  StyledDeleteButton,
} from "./styles";
import { PlusOutlined } from "@ant-design/icons";

export interface GistFile {
  filename: string;
  content: string;
}

type Props = {
  files: GistFile[];
  setFiles: (file: GistFile[]) => void;
  description: string;
  setDescription: (description: string) => void;
  asyncFunction: () => Promise<void>;
  isGistCreated: boolean;
  message: string;
};

export function GistForm({
  files,
  setFiles,
  description,
  setDescription,
  asyncFunction,
  isGistCreated,
  message,
}: Props) {
  const { TextArea } = Input;
  const [form] = Form.useForm();
  const buttonItemLayout = { wrapperCol: { span: 1, offset: 0 } };

  const addFile = () => {
    setFiles([...files, { filename: "", content: "" }]);
  };

  const removeFile = (idx: number) => {
    setFiles(files.filter((file) => files.indexOf(file) !== idx));
  };

  const updateFile = (index: number, field: keyof GistFile, value: string) => {
    const newFiles = [...files];
    newFiles[index][field] = value;
    setFiles(newFiles);
  };

  const isFormEmpty = () => {
    const fileCount = files.length;
    let isAnyFileNameEmpty = false;
    for (let i = 0; i < fileCount; i++) {
      isAnyFileNameEmpty = isAnyFileNameEmpty || files[i].filename.length === 0;
    }

    return isAnyFileNameEmpty || !description;
  };

  return (
    <>
      <StyledForm
        labelCol={{ span: 4 }}
        wrapperCol={{ span: 14 }}
        layout="horizontal"
        form={form}
      >
        <StyledFormItem rules={[{ required: true }]}>
          <Input
            placeholder="Enter gist description..."
            type="text"
            value={description}
            onChange={(e) => setDescription(e.target.value)}
          />
        </StyledFormItem>
        {files.map((file, index) => (
          <div key={index}>
            <StyledFormItem rules={[{ required: true }]}>
              <Row>
                <Col span={index === 0 ? 24 : 23}>
                  <Input
                    placeholder="Enter file name..."
                    type="text"
                    value={file.filename}
                    onChange={(e) =>
                      updateFile(index, "filename", e.target.value)
                    }
                  />
                </Col>
                <Col span={index === 0 ? 0 : 1} offset={0}>
                  {index >= 1 ? (
                    <StyledDeleteButton onClick={() => removeFile(index)} />
                  ) : (
                    <></>
                  )}
                </Col>
              </Row>
            </StyledFormItem>
            <StyledFormItem rules={[{ required: true }]}>
              <TextArea
                rows={8}
                placeholder="Enter file content..."
                value={file.content}
                onChange={(e) => updateFile(index, "content", e.target.value)}
              />
            </StyledFormItem>
          </div>
        ))}

        <StyledFormItem {...buttonItemLayout}>
          <StyledButton onClick={addFile}>
            <PlusOutlined />
            Add File
          </StyledButton>
        </StyledFormItem>
        <StyledFormItem {...buttonItemLayout}>
          <StyledButton onClick={asyncFunction} disabled={isFormEmpty()}>
            Create Gist
          </StyledButton>
        </StyledFormItem>
      </StyledForm>
      {isGistCreated && <Alert message={message} type="success" />}
    </>
  );
}
