import styled from "styled-components";
import { Divider } from "antd";

export const StyledDiv = styled.div`
  display: flex;
  flex-direction: row;

  justify-content: right;
  padding-left: 10px;
  color: #04aa6d;
`;

export const StyledDivider = styled(Divider)`
  margin: 10px 0 5px 0;
  background-color: lightgray;
  height: 25px;
  width: 2px;
`;
