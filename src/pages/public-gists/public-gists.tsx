import { useState } from "react";
import { GridLayout } from "../../layouts/grid/grid";
import { ListLayout } from "../../layouts/list/list";
import { Footer } from "../../components/footer/footer";
import { StyledDiv, StyledDivider } from "./styles";
import { GridButton } from "../../components/buttons/layout-switch-buttons/grid-button";
import { ListButton } from "../../components/buttons/layout-switch-buttons/list-button";

interface State {
  isGridLayout: boolean;
}

const initialState: State = {
  isGridLayout: false,
};

export function PublicGists() {
  const [state, setState] = useState<State>(initialState);
  const [isEmpty, setIsEmpty] = useState<boolean>(false);

  return (
    <>
      <StyledDiv>
        <GridButton
          isClicked={state.isGridLayout}
          onClick={() => setState({ ...state, isGridLayout: true })}
        />

        <StyledDivider type="vertical" />

        <ListButton
          isClicked={state.isGridLayout}
          onClick={() => setState({ ...state, isGridLayout: false })}
        />
      </StyledDiv>

      {state.isGridLayout ? (
        <GridLayout isEmpty={isEmpty} />
      ) : (
        <ListLayout setIsEmpty={setIsEmpty} />
      )}

      {isEmpty ? <></> : <Footer />}
    </>
  );
}
