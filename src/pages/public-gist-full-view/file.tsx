import { Col, Row } from "antd";
import { GistStored } from "../../reducer/reducer";
import { ProfileFileType } from "./styles";
import { useState, useEffect } from "react";
import { useSelector } from "react-redux";
import axios from "axios";
import gistIcon from "../../assets/icons/gist.png";
import { StyledGistIcon, StyledFile } from "../user-gists/styles";
import SyntaxHighlighter from "react-syntax-highlighter";
import { a11yLight } from "react-syntax-highlighter/dist/esm/styles/hljs";
import { EnableBoxHighlight } from "../../hooks/enableBoxHighlight";

export function File() {
  const [file, setFile] = useState<string>("");
  const allGists = useSelector((state: GistStored) => state.publicGists);
  const gistId = useSelector((state: GistStored) => state.clickedGistId);
  const gistData = allGists[allGists.findIndex((g) => g.id === gistId)];

  const hover = EnableBoxHighlight();

  useEffect(() => {
    const fetchGists = async () => {
      const response = await axios.get(gistData.files.raw_url);

      let gistFile = response.data;

      try {
        response.data.split("\n");
      } catch {
        const jsonString = JSON.stringify(response.data, null, 2);
        gistFile = jsonString.replace(/\\n/g, "\n");
      }

      if (gistData !== undefined && Object.keys(gistData).length !== 0) {
        setFile(gistFile);
      }
    };
    fetchGists();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <StyledFile hover={hover}>
      <Row style={{ marginTop: "5px", marginBottom: "-5px" }}>
        <Col>
          <StyledGistIcon src={gistIcon} alt="logo" />
        </Col>
        <ProfileFileType>
          {" "}
          {gistData.files.filename}.{gistData.files.filetype}{" "}
        </ProfileFileType>
      </Row>
      <hr style={{ backgroundColor: "lightgray" }}></hr>
      <SyntaxHighlighter
        language="javascript"
        style={a11yLight}
        wrapLines={true}
        showLineNumbers={true}
        wrapLongLines={true}
      >
        {file}
      </SyntaxHighlighter>
    </StyledFile>
  );
}
