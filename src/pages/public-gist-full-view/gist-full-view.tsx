import { Header } from "./header";
import { File } from "./file";
import { StyledGistPage } from "./styles";

export function PublicGistFullView() {
  return (
    <StyledGistPage>
      <Header />
      <File />
    </StyledGistPage>
  );
}
