import { Col, Row } from "antd";
import { useSelector } from "react-redux";
import { GistStored } from "../../reducer/reducer";
import {
  StyledRef,
  StyledImage,
  ProfileName,
  ProfileTime,
  StyledUserInfo,
} from "./styles";
import { ForkButton } from "../../components/buttons/git-actions/fork-button";
import { StarButton } from "../../components/buttons/git-actions/star-button";
import { getTimeElapsed } from "../../hooks/getTimeElapsed";

export function Header() {
  const allGists = useSelector((state: GistStored) => state.publicGists);
  const gistId = useSelector((state: GistStored) => state.clickedGistId);
  const gistData = allGists[allGists.findIndex((g) => g.id === gistId)];

  return (
    <Row>
      <Col span={16}>
        <StyledUserInfo>
          <div>
            <StyledRef href={gistData.url} target="_blank" rel="noreferrer">
              <StyledImage
                src={gistData.owner.avatar_url}
                alt="Gist Profile picture"
              />
            </StyledRef>
          </div>
          <div>
            <ProfileName>
              {gistData.owner.login}/{" "}
              <strong>
                {gistData.files.filename}.{gistData.files.filetype}
              </strong>
            </ProfileName>

            <ProfileTime>Created {getTimeElapsed(gistData.owner.time)} ago</ProfileTime>
          </div>
        </StyledUserInfo>
      </Col>

      <Col span={4} offset={4}>
        <Row>
          <Col span={12}>
            <StarButton id={gistId} preview={false} />
          </Col>
          <Col span={12}>
            <ForkButton id={gistId} preview={false} />
          </Col>
        </Row>
      </Col>
    </Row>
  );
}
