import styled from "styled-components";
import { Row, Col } from "antd";

type Props = {
  clicked: boolean;
};

export const StyledGistPage = styled.div`
  margin-left: 120px;
  margin-right: 120px;
  margin-top: 60px;
`;

export const StyledDisplay = styled(Row)`
  margin-top: 10px;
  padding-top: 10px;
`;

export const ProfileInfo = styled.div`
  text-align: left;
`;

export const ProfileName = styled.div`
  font-size: 20px;
  color: #0969da;
`;

export const ProfileTime = styled.div`
  font-size: 16px;
  color: darkgray;
  margin-top: 5px;
`;

export const ProfileFileType = styled(Col)`
  font-size: 16px;
  color: gray;
  margin-bottom: 4px;
  color: #0969da;
`;

export const StyledRef = styled.a`
  text-decoration: none;
  color: #04aa6d;
  font-weight: bold;
`;

export const StyledImage = styled.img`
  width: 60px;
  height: 60px;
  border-radius: 50%;
`;

export const StyledFile = styled.div`
  border: solid 1px lightgray;
  text-align: left;
  margin-top: 30px;
  line-height: 20px;
`;

export const StyledForkButton = styled.button`
  color: ${(props: Props) => (props.clicked ? "orange" : "#04AA6D")};
  background-color: white;
  border: none;
  font-size: 16px;
`;

export const StyledButtonText = styled(Col)`
  color: #04aa6d;
  font-size: 16px;
`;

export const StyledStarButton = styled.button`
  color: ${(props: Props) => (props.clicked ? "orange" : "#04AA6D")};
  background-color: white;
  border: none;
  font-size: 16px;
`;

export const StyledUserInfo = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  text-align: left;
  gap: 10px;
`;
