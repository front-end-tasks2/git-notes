import { useState } from "react";
import { GistFile, GistForm } from "../../components/gist-form/gist-form";

export function CreateGist() {
  const [files, setFiles] = useState<GistFile[]>([
    { filename: "", content: "" },
  ]);
  const [description, setDescription] = useState("");
  const [isGistCreated, setIsGistCreated] = useState<boolean>(false);

  const createGist = async () => {
    const data = {
      description: description,
      public: true,
      files: files.reduce(
        (acc, curr) => ({
          ...acc,
          [curr.filename]: { content: curr.content },
        }),
        {}
      ),
    };

    try {
      const response = await fetch("https://api.github.com/gists", {
        method: "POST",
        body: JSON.stringify(data),
        headers: {
          Authorization: "Bearer " + localStorage.getItem("accessToken"),
          Accept: "application/vnd.github+json",
        },
      });
      console.log(response);
    } catch (error) {
      console.error(error);
    }
    setIsGistCreated(true);
  };

  return (
    <GistForm
      files={files}
      setFiles={setFiles}
      description={description}
      setDescription={setDescription}
      asyncFunction={createGist}
      isGistCreated={isGistCreated}
      message={"New gist created"}
    />
  );
}
