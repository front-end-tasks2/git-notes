import { useState } from "react";
import { GistStored } from "../../reducer/reducer";
import { useSelector } from "react-redux";
import { GistForm } from "../../components/gist-form/gist-form";

interface GistFile {
  filename: string;
  content: string;
}

interface GistFile {
  filename: string;
  content: string;
}

export function EditGist() {
  const userGists = useSelector((state: GistStored) => state.userGists);
  const gistId = useSelector((state: GistStored) => state.editGistId);

  const gistToEdit =
    userGists[userGists.findIndex((gist) => gist.id === gistId)];

  const gistFilename = Object.keys(gistToEdit.files)[0];
  const gistFileContent = gistToEdit.files[gistFilename].content;

  const [files, setFiles] = useState<GistFile[]>([
    {
      filename: gistFilename,
      content: gistFileContent,
    },
  ]);
  const [description, setDescription] = useState(gistToEdit.description);
  const [isGistCreated, setIsGistCreated] = useState<boolean>(false);

  const editGist = async () => {
    const data = {
      description: description,
      public: true,
      files: files.reduce(
        (acc, curr) => ({
          ...acc,
          [curr.filename]: { content: curr.content },
        }),
        {}
      ),
    };

    try {
      const response = await fetch("https://api.github.com/gists/" + gistId, {
        method: "PATCH",
        body: JSON.stringify(data),
        headers: {
          Authorization: "Bearer " + localStorage.getItem("accessToken"),
          Accept: "application/vnd.github+json",
        },
      });
      console.log(response);
    } catch (error) {
      console.error(error);
    }
    setIsGistCreated(true);
  };

  return (
    <GistForm
      files={files}
      setFiles={setFiles}
      description={description}
      setDescription={setDescription}
      asyncFunction={editGist}
      isGistCreated={isGistCreated}
      message={"Gist updated successfully"}
    />
  );
}
