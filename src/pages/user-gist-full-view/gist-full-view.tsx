import { useSelector } from "react-redux";
import { GistStored } from "../../reducer/reducer";
import { UserGist } from "../user-gists/user-gist";

export function UserGistFullView() {
  const userGists = useSelector((state: GistStored) => state.userGists);
  const gistId = useSelector((state: GistStored) => state.clickedGistId);

  const gist = userGists[userGists.findIndex((g) => g.id === gistId)];

  return <UserGist gist={gist} preview={false} />;
}
