import styled from "styled-components";
import { Divider, Row } from "antd";

export const StyledPage = styled(Row)``;

type Props = {
  dimensions: number;
};

export const StyledUserImage = styled.img`
  height: ${(props: Props) => props.dimensions / 5}px;
  width: ${(props: Props) => props.dimensions / 5}px;
  border-radius: 50%;
  border: none;
  background-color: #efefef;
`;

export const StyledProfileCol = styled.div`
  padding-left: 10px;
  padding-top: 50px;
  display: flex;
  flex-direction: column;
  flex-wrap: wrap;
  align-items: inherit;
`;

export const StyledUserName = styled(Row)`
  padding-top: 30px;
  font-size: 24px;
  font-wieght: bolder;
  justify-content: center;
`;

export const StyledProfileButton = styled.div`
  margin-top: 30px;
  background-color: white;
  border: solid 1px lightgray;
  border-radius: 5px;
  font-size: 16px;
  padding: 6px;
  align-items: center;
`;

export const StyledListEntryURL = styled.a`
  color: #0969da;
  text-decoration: none;
  padding-top: 10px;
  padding-bottom: 10px;
`;

export const StyledDivider = styled(Divider)`
  width: 1px;
  background-color: lightgray;
  left: 50%;
  height: ${(props: Props) => props.dimensions}px;
`;
