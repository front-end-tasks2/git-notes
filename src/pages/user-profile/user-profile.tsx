import { Col, Divider } from "antd";
import { useSelector } from "react-redux";
import { GistStored } from "../../reducer/reducer";
import {
  StyledPage,
  StyledUserImage,
  StyledUserName,
  StyledProfileButton,
  StyledListEntryURL,
  StyledDivider,
} from "./styles";
import { useState, useEffect } from "react";

import { UserGistsList } from "../user-gists/user-gists-list";

export function UserProfilePage() {
  const userProfile = useSelector((state: GistStored) => state.userProfile);

  const [dimensions, setDimensions] = useState({
    width: window.innerWidth,
    height: window.innerHeight,
  });
  useEffect(() => {
    function handleResize() {
      setDimensions({
        width: window.innerWidth,
        height: window.innerHeight,
      });
    }

    window.addEventListener("resize", handleResize);
  });

  return (
    <>
      <StyledPage>
        <Divider orientation="left" plain></Divider>

        <Col span={6}>
          <div>
            <StyledUserImage
              dimensions={dimensions.width}
              src={userProfile.avatar_url}
              alt=""
            />
          </div>
          <div>
            <StyledUserName>{userProfile.name}</StyledUserName>
            <StyledProfileButton>
              <StyledListEntryURL
                target="_blank"
                rel="noopener noreferrer"
                href={userProfile.html_url}
              >
                View Github Profile
              </StyledListEntryURL>
            </StyledProfileButton>
          </div>
        </Col>
        <Col span={1} offset={1}>
          <StyledDivider type="vertical" dimensions={dimensions.width}/>
        </Col>

        <Col span={15}>
          <UserGistsList preview={true} />
        </Col>
        <Divider></Divider>
      </StyledPage>
    </>
  );
}
