import styled from "styled-components";
import { Row, Col } from "antd";

type Props = {
  clicked?: boolean;
  preview?: boolean;
};

type FileProps = {
  hover: boolean;
};


export const StyledDisplay = styled(Row)`
  margin-top: 10px;
  padding-top: 10px;
`;

export const StyledUserInfo = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
`;

export const ProfileInfo = styled.div`
  text-align: left;
  padding-left: 10px;
  font-weight: normal;
`;

export const ProfileName = styled.div`
  font-size: 16px;
  color: #0969da;
  margin-bottom: 2px;
`;

export const ProfileTime = styled.div`
  font-size: 16px;
  color: darkgray;
  margin-bottom: 2px;
  margin-top: 5px;
`;

export const ProfileFileType = styled(Col)`
  font-size: 16px;
  color: gray;
  margin-bottom: 4px;
  color: #0969da;
`;

export const StyledRef = styled.a`
  text-decoration: none;
  color: #04aa6d;
`;

export const StyledImage = styled.img`
  width: ${(props: Props) => (props.preview ? "40px" : "60px")};
  height: ${(props: Props) => (props.preview ? "40px" : "60px")};
  border-radius: 50%;
  gap: 10px;
  margin-top: -10px;
`;

export const StyledFile = styled.div`
  border: solid 1px lightgray;
  border-radius: 10px;
  text-align: left;
  line-height: 20px;
  box-shadow: 1px 2px 9px lightgray;
  margin-top: 10px;
  padding-bottom: 10px;
  &:hover {
    border: ${(props: FileProps) => (props.hover ? "1px solid #0969da" : "solid 1px lightgray")};
  }
`;

export const StyledButtonText = styled(Col)`
  color: #04aa6d;
  font-size: 16px;
`;

export const StyledActionButton = styled.button`
  color: ${(props: Props) => (props.clicked ? "orange" : "#0969da")};
  background-color: white;
  border: none;
  font-size: 16px;
  cursor: pointer;
  &:hover {
    background-color: lightblue;
  }
`;

export const StyledGistSection = styled.div`
  margin-top: ${(props: Props) => (props.preview ? "50px" : "100px")};
  margin-bottom: 30px;
  padding: 10px;
`;

export const StyledHeaderRow = styled(Row)`
  margin-top: 10px;
`;

export const StyledCreateGistButton = styled.button`
  font-size: inherit;
  border: none;
  background-color: white;
  color: #0969da;
`;

export const StyledUserGistClickable = styled.button`
  border: none;
  background-color: white;
  color: #04aa6d;
`;

export const StyledGistIcon = styled.img`
  width: 20px;
  height: 20px;
  margin-left: 10px;
  margin-right: 5px;
`;
