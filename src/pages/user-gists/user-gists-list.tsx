import { useSelector, useDispatch } from "react-redux";
import { StyledCreateGistButton } from "./styles";
import {
  GistStored,
  setCurrentScreen,
  Screen,
  storeUserGists,
} from "../../reducer/reducer";
import useRestAPI from "../../hooks/useRestAPI";
import { UserGist } from "./user-gist";
import { Alert, Spin } from "antd";

interface Props {
  preview: boolean;
}

export function UserGistsList({ preview }: Props) {
  const gists = useSelector((state: GistStored) => state.userGists);
  const dispatch = useDispatch();

  const { data, isLoading, isError } = useRestAPI("gists");

  if (isLoading) {
    console.log("Loading");
  } else if (data) {
    dispatch(storeUserGists(data));
  } else if (isError) {
    console.log("Error");
  }

  const renderData = data ? (
    <>
      {gists.map((gist) => (
        <UserGist key={gist.id} gist={gist} preview={preview} />
      ))}
      {gists.length ? (
        <></>
      ) : (
        <div style={{ marginTop: "100px" }}>
          <h1>You don’t have any gists yet</h1>
          <h2>
            <StyledCreateGistButton
              onClick={() => dispatch(setCurrentScreen(Screen.CreateNewGist))}
            >
              Create a gist
            </StyledCreateGistButton>{" "}
            to get started
          </h2>
        </div>
      )}
    </>
  ) : (
    <Alert message="Error loading user gists" type="error" />
  );

  return (
    <div>
      {isLoading ? <Spin tip="Loading" size="large" style={{marginTop:"200px"}}></Spin> : <>{renderData}</>}
    </div>
  );
}
