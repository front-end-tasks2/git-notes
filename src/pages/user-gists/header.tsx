import { Col } from "antd";
import { useSelector } from "react-redux";
import { GistStored, UserGistType } from "../../reducer/reducer";
import {
  StyledImage,
  ProfileName,
  ProfileInfo,
  ProfileTime,
  StyledHeaderRow,
  StyledUserInfo,
} from "./styles";
import { DeleteButton } from "../../components/buttons/git-actions/delete-button";
import { EditButton } from "../../components/buttons/git-actions/edit-button";
import { StarButton } from "../../components/buttons/git-actions/star-button";
import { getTimeElapsed } from "../../hooks/getTimeElapsed";

interface Props {
  gist: UserGistType;
  fileName: string;
  preview: boolean;
}

export function Header({ gist, fileName, preview }: Props) {
  const userProfile = useSelector((state: GistStored) => state.userProfile);

  return (
    <StyledHeaderRow>
      <Col span={12}>
        <strong>
          <StyledUserInfo>
            <StyledImage
              preview={preview}
              src={userProfile.avatar_url}
              alt="Gist Profile picture"
            />
            <ProfileInfo>
              <ProfileName>
                {userProfile.login}/ <strong>{fileName}</strong>
              </ProfileName>

              <ProfileTime>
                Created {getTimeElapsed(gist.created_at)} ago
              </ProfileTime>
            </ProfileInfo>
          </StyledUserInfo>
        </strong>
      </Col>

      <Col span={6} offset={6}>
        <EditButton gist={gist} preview={preview} />

        <DeleteButton gist={gist} preview={preview} />

        <StarButton id={gist.id} preview={preview} />
      </Col>
    </StyledHeaderRow>
  );
}
