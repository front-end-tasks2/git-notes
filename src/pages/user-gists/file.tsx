import { Col, Row } from "antd";
import { UserGistType, storeUserGistContent } from "../../reducer/reducer";
import { ProfileFileType, StyledFile, StyledGistIcon } from "./styles";
import { useState, useEffect } from "react";
import axios from "axios";
import { useDispatch } from "react-redux";
import gistIcon from "../../assets/icons/gist.png";
import SyntaxHighlighter from "react-syntax-highlighter";
import { a11yLight } from "react-syntax-highlighter/dist/esm/styles/hljs";
import { EnableBoxHighlight } from "../../hooks/enableBoxHighlight";

interface Props {
  gist: UserGistType;
  fileName: string;
  preview: boolean;
}

export function File({ gist, fileName, preview }: Props) {
  const dispatch = useDispatch();
  const [lines, setLines] = useState<string[]>([""]);

  const hover = EnableBoxHighlight();

  useEffect(() => {
    const fetchGists = async () => {
      const response = await axios.get(gist.files[fileName].raw_url);
      const gistFile: string = response.data;

      if (gistFile !== undefined && Object.keys(gistFile).length !== 0) {
        const fullGist = gistFile.split("\n");

        if (preview) {
          const emptyArray = new Array(3).fill("");
          const previewLines = [...fullGist, ...emptyArray];
          if (previewLines.length > 3) setLines(previewLines.slice(0, 3));
        } else {
          setLines(fullGist);
        }
        dispatch(
          storeUserGistContent({
            id: gist.id,
            filename: fileName,
            content: fullGist.join("\n"),
          })
        );
      }
    };
    fetchGists();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <StyledFile hover={hover}>
      <Row style={{ marginTop: "5px", marginBottom: "-5px" }}>
        <Col>
          <StyledGistIcon src={gistIcon} alt="logo" />
        </Col>
        <ProfileFileType> {gist.description || "Untitled"} </ProfileFileType>
      </Row>
      <hr style={{ backgroundColor: "lightgray" }}></hr>
      <SyntaxHighlighter
        language="javascript"
        style={a11yLight}
        wrapLines={true}
        showLineNumbers={true}
        wrapLongLines={true}
      >
        {lines.join("\n")}
      </SyntaxHighlighter>
    </StyledFile>
  );
}
