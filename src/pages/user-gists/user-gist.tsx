import {
  UserGistType,
  setCurrentScreen,
  Screen,
  setCurrentGist,
} from "../../reducer/reducer";
import { Header } from "./header";
import { File } from "./file";
import { StyledGistSection } from "./styles";
import { useDispatch } from "react-redux";

interface GistItemProps {
  gist: UserGistType;
  preview: boolean;
}

export function UserGist({ gist, preview }: GistItemProps) {
  const dispatch = useDispatch();

  return (
    <div>
      <ul>
        {Object.keys(gist.files).map((fileName) => (
          <StyledGistSection preview={preview}>
            <Header gist={gist} fileName={fileName} preview={preview} />
            <div
              onClick={() => {
                dispatch(setCurrentScreen(Screen.ClickedUserGist));
                dispatch(setCurrentGist(gist.id));
              }}
            >
              <File gist={gist} fileName={fileName} preview={preview} />
            </div>
          </StyledGistSection>
        ))}
      </ul>
    </div>
  );
}
