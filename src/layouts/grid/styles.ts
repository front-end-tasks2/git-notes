import styled from "styled-components";
import { Col, Row } from "antd";

type Props = {
  hover: boolean;
};

export const StyledImage = styled.img`
  width: 20px;
  height: 20px;
  border-radius: 50%;
  padding-left: 5px;
  margin-top: 4px;
`;

export const StyledGrid = styled(Row)`
  border: none;
  margin-top: 20px;
`;

export const StyledGridItem = styled(Col)`
  word-break: break-all;
`;

export const StyledTextBox = styled.div`
  box-shadow: 1px 2px 9px lightgray;
  border-radius: 5px;
  padding-top: 20px;
  &:hover {
    border: ${(props: Props) => (props.hover ? "1px solid #0969da" : "none")};
  }
`;

export const StyledText = styled.div`
  font-family: monospace;
  align-items: left;
  text-align: left;
  font-size: 12px;
  padding-left: 2%;
`;

export const Footer = styled.footer`
  display: flex;
  flex-direction: row;
  margin-left: 3%;
  padding-bottom: 20px;
`;

export const ProfileInfo = styled.div`
  display: flex;
  flex-direction: column;
  margin-left: 5px;
  text-align: left;
  margin-top: 4px;
`;

export const ProfileName = styled.div`
  font-size: 10px;
  color: #0969da;
  margin-bottom: 2px;
  margin-right: 2%;
`;

export const ProfileTime = styled.div`
  font-size: 10px;
  color: darkgray;
  font-weight: bold;
  margin-bottom: 2px;
`;

export const ProfileFileType = styled.div`
  font-size: 10px;
  color: gray;
  margin-bottom: 4px;
`;
