import { StyledGrid } from "./styles";
import { useSelector } from "react-redux";
import { GistStored } from "../../reducer/reducer";
import { GridItems } from "./grid-items";

export function GridFullPage() {
  const newLogEntry = useSelector((state: GistStored) => state.publicGists);

  return (
    <>
      <StyledGrid gutter={[{ xs: 8, sm: 16, md: 24, lg: 32 }, 24]}>
        {newLogEntry.map((newLogEntry, index) => (
          <GridItems newLogEntry={newLogEntry} index={index} span={8} />
        ))}
      </StyledGrid>
    </>
  );
}
