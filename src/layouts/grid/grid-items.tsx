import {
  StyledImage,
  StyledGridItem,
  StyledText,
  StyledTextBox,
  Footer,
  ProfileInfo,
  ProfileName,
  ProfileFileType,
  ProfileTime,
} from "./styles";
import { StyledRef, StyledDispatchButton } from "../list/styles";
import {
  Gist,
  setCurrentScreen,
  storeGistFile,
  Screen,
  setCurrentGist,
} from "../../reducer/reducer";
import axios from "axios";
import { useState, useEffect } from "react";
import { useDispatch } from "react-redux";
import { getTimeElapsed } from "../../hooks/getTimeElapsed";
import SyntaxHighlighter from "react-syntax-highlighter";
import { a11yLight } from "react-syntax-highlighter/dist/esm/styles/hljs";
import { EnableBoxHighlight } from "../../hooks/enableBoxHighlight";

type Props = {
  newLogEntry: Gist;
  index: number;
  span: number;
};

export function GridItems({ newLogEntry, index, span }: Props) {
  const file_url: string = newLogEntry.files.raw_url;
  const gist_id: string = newLogEntry.id;
  const dispatch = useDispatch();

  const [preview, setPreview] = useState<string>("");

  const hover = EnableBoxHighlight();

  useEffect(() => {
    const fetchGists = async () => {
      const response = await axios.get(file_url);
      let gistsData = response.data;

      try {
        response.data.split("\n");
      } catch {
        const jsonString = JSON.stringify(response.data, null, 2);
        gistsData = jsonString.replace(/\\n/g, "\n");
        console.log("File format not convertible to string");
      }

      if (gistsData !== undefined && Object.keys(gistsData).length !== 0) {
        setPreview(createFilePreview(gistsData));
        dispatch(storeGistFile({ id: gist_id, file_data: gistsData }));
      }
    };
    fetchGists();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [file_url]);

  return (
    <StyledGridItem span={span}>
      <StyledTextBox hover={hover}>
        <StyledText>
          <StyledDispatchButton
            preview
            onClick={() => {
              dispatch(setCurrentScreen(Screen.ClickedPublicGist));
              dispatch(setCurrentGist(newLogEntry.id));
            }}
          >
            <SyntaxHighlighter
              language="javascript"
              style={a11yLight}
              wrapLines={true}
              showLineNumbers={true}
              wrapLongLines={true}
            >
              {preview}
            </SyntaxHighlighter>
          </StyledDispatchButton>
        </StyledText>

        <hr style={{ width: "90%", backgroundColor: "gray" }}></hr>
        <Footer>
          <StyledRef href={newLogEntry.url} target="_blank" rel="noreferrer">
            <StyledImage
              src={newLogEntry.owner.avatar_url}
              alt="Gist Profile picture"
            />
          </StyledRef>
          <ProfileInfo>
            <StyledRef href={newLogEntry.url} target="_blank" rel="noreferrer">
              <ProfileName>
                {newLogEntry.owner.login}/{" "}
                <strong>
                  {newLogEntry.files.filename}.{newLogEntry.files.filetype}
                </strong>
              </ProfileName>
            </StyledRef>
            <ProfileTime>
              Created {getTimeElapsed(newLogEntry.owner.time)} ago
            </ProfileTime>
            <ProfileFileType> {newLogEntry.files.type} </ProfileFileType>
          </ProfileInfo>
        </Footer>
      </StyledTextBox>
    </StyledGridItem>
  );
}

function createFilePreview(data: string): string {
  if (data === undefined || data === null || data.length === 0) {
    console.error("No data input to createFilePreview function");
    return "";
  }
  try {
    const lines = data.split("\n");

    let newlines = lines.map((line: string) => {
      let l = line;
      if (line.length > 20) {
        l = line.slice(0, 20);
        l = l.concat("...");
      }
      return l;
    });

    const emptyArray = new Array(9).fill("");
    newlines = [...newlines, ...emptyArray];

    if (newlines.length > 9) newlines = newlines.slice(0, 9);

    return newlines.join("\n");
  } catch {
    console.error("Catch in creating file preview");
    return "";
  }
}
