import { useSelector } from "react-redux";
import { GistStored, Gist } from "../../reducer/reducer";
import { StyledGrid } from "./styles";
import { Empty } from "antd";
import { GridFiltered } from "./grid-filtered";
import { GridFullPage } from "./grid-full-page";
import { useState, useEffect } from "react";

type Props = {
  isEmpty: boolean;
};

export function GridLayout({ isEmpty }: Props) {
  const gists = useSelector((state: GistStored) => state.publicGists);
  const [filteredResults, setFilteredResults] = useState<Gist[]>();
  const [searchInput, setSearchInput] = useState("");

  const query = useSelector((state: GistStored) => state.query);

  const searchItems = (searchValue: string) => {
    setSearchInput(searchValue);
    if (searchInput !== "") {
      const filteredData = gists?.filter((item) => {
        return Object.values(item)
          .join("")
          .toLowerCase()
          .includes(searchInput.toLowerCase());
      });
      setFilteredResults(filteredData);
    } else {
      setFilteredResults(gists);
    }
  };

  useEffect(() => {
    searchItems(query);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [query]);

  return (
    <>
      {isEmpty ? (
        <Empty
          style={{ margin: "200px 0 100px 0" }}
          description={<span>You must be logged in to view this page.</span>}
        />
      ) : (
        <StyledGrid>
          <>
            {searchInput.length > 1 ? (
              <GridFiltered filteredResults={filteredResults} />
            ) : (
              <>
                {gists !== undefined && Object.keys(gists).length !== 0 ? (
                  <GridFullPage />
                ) : (
                  <>
                    <Empty style={{ margin: "100px 0 100px 40%" }} />
                  </>
                )}
              </>
            )}
          </>
        </StyledGrid>
      )}
    </>
  );
}
