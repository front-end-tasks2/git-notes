import { Gist } from "../../reducer/reducer";
import { StyledGrid } from "./styles";
import { GridItems } from "./grid-items";

type Props = {
  filteredResults: Gist[] | undefined;
};

export function GridFiltered({ filteredResults }: Props) {
  return (
    <StyledGrid gutter={[16, 24]}>
      {filteredResults?.map((newLogEntry, index) => (
        <GridItems newLogEntry={newLogEntry} index={index} span={24} />
      ))}
    </StyledGrid>
  );
}
