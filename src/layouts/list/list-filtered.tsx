import { Gist } from "../../reducer/reducer";
import { ListItems } from "./list-items";

type Props = {
  filteredResults: Gist[] | undefined;
};

export function ListFiltered({ filteredResults }: Props) {
  return (
    <>
      {filteredResults?.map((newLogEntry, index) => (
        <ListItems newLogEntry={newLogEntry} index={index} />
      ))}
    </>
  );
}
