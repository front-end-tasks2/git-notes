import { useState, useEffect } from "react";
import { Gist, GistStored, storePublicGists } from "../../reducer/reducer";
import { ListLayoutHeader } from "./list-header";
import { ListFullPage } from "./list-full-page";
import { StyledTable } from "./styles";
import { ListFiltered } from "./list-filtered";
import { useSelector, useDispatch } from "react-redux";
import { Spin, Alert, Empty } from "antd";
import useRestAPI from "../../hooks/useRestAPI";
import getPublicGistsData from "../../hooks/getPublicGistData";

type Props = {
  setIsEmpty: (bool: boolean) => void;
};

export function ListLayout({ setIsEmpty }: Props) {
  const [gists, setGists] = useState<Gist[]>();
  const dispatch = useDispatch();

  const [filteredResults, setFilteredResults] = useState<Gist[]>();
  const [searchInput, setSearchInput] = useState("");

  const { badCredentials, data, isLoading, isError } =
    useRestAPI(`gists/public`);
  const isLoggedIn: boolean = useSelector(
    (state: GistStored) => state.userProfile.logged_in
  );

  useEffect(() => {
    console.log("Bad cred: ", badCredentials);
    if (!badCredentials) {
      setIsEmpty(false);

      if (isLoading) {
        console.log("Loading");
      } else if (isError) {
        console.log("Error");
      } else if (data) {
        const publicGistData = getPublicGistsData(data);
        dispatch(storePublicGists(publicGistData));
        setGists(publicGistData);
      }
    } else {
      setIsEmpty(true);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isLoggedIn, data, badCredentials]);

  const query = useSelector((state: GistStored) => state.query);

  const searchItems = (searchValue: string) => {
    setSearchInput(searchValue);
    if (searchInput !== "") {
      const filteredData = gists?.filter((item) => {
        return Object.values(item)
          .join("")
          .toLowerCase()
          .includes(searchInput.toLowerCase());
      });
      setFilteredResults(filteredData);
    } else {
      setFilteredResults(gists);
    }
  };

  useEffect(() => {
    searchItems(query);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [query]);

  const renderData = data ? (
    <>
      {searchInput.length > 1 ? (
        <ListFiltered filteredResults={filteredResults} />
      ) : (
        <>
          {gists !== undefined && Object.keys(gists).length !== 0 ? (
            <ListFullPage />
          ) : (
            <>
              <Spin
                tip="Loading"
                size="large"
                style={{ marginTop: "200px" }}
              ></Spin>
            </>
          )}
        </>
      )}
    </>
  ) : (
    <Alert message="Error loading gist data" type="error" />
  );

  return (
    <>
      <StyledTable>
        <ListLayoutHeader />
        <>
          {badCredentials ? (
            <Empty
              style={{ margin: "200px 0 100px 0" }}
              description={
                <span>You must be logged in to view this page.</span>
              }
            />
          ) : (
            <>
              {isLoading ? (
                <Spin
                  tip="Loading"
                  size="large"
                  style={{ margin: "200px 0 100px 0" }}
                ></Spin>
              ) : (
                <>{renderData}</>
              )}
            </>
          )}
        </>
      </StyledTable>
    </>
  );
}
