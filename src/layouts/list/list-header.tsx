import { StyledHeaderEntry, StyledBox, StyledTableHeading } from "./styles";
import { gistTableSpan } from "../../constants/gistListTableSpan";

export function ListLayoutHeader() {
  return (
    <StyledTableHeading>
      <StyledHeaderEntry span={gistTableSpan[0]}>
        <StyledBox />
      </StyledHeaderEntry>

      <StyledHeaderEntry span={gistTableSpan[1]}></StyledHeaderEntry>
      <StyledHeaderEntry span={gistTableSpan[2]}>Name</StyledHeaderEntry>
      <StyledHeaderEntry span={gistTableSpan[3]}>Date</StyledHeaderEntry>
      <StyledHeaderEntry span={gistTableSpan[4]}>Time</StyledHeaderEntry>
      <StyledHeaderEntry span={gistTableSpan[5]}>Keyword</StyledHeaderEntry>
      <StyledHeaderEntry span={gistTableSpan[6]}>Notebook Name</StyledHeaderEntry>
      <StyledHeaderEntry span={gistTableSpan[7]}></StyledHeaderEntry>
      <StyledHeaderEntry span={gistTableSpan[8]}></StyledHeaderEntry>
    </StyledTableHeading>
  );
}
