import {
  StyledGridEntry,
  StyledRef,
  StyledImage,
  StyledBox,
  StyledDispatchButton,
  StyledButtonsDiv,
  StyledGrid
} from "./styles";
import {
  Gist,
  setCurrentGist,
  setCurrentScreen,
  Screen,
} from "../../reducer/reducer";
import { useDispatch } from "react-redux";
import { ForkButton } from "../../components/buttons/git-actions/fork-button";
import { StarButton } from "../../components/buttons/git-actions/star-button";
import { gistTableSpan } from "../../constants/gistListTableSpan";

type Props = {
  newLogEntry: Gist;
  index: number;
};

export function ListItems({ newLogEntry, index }: Props) {
  const dispatch = useDispatch();

  const time = newLogEntry.owner.time.split("T")[0];
  const date = newLogEntry.owner.time.split("T")[1].split("Z");

  return (
    <>
    <StyledGrid gutter={16}>
      <StyledGridEntry span={gistTableSpan[0]}>
        <StyledBox />
      </StyledGridEntry>

      <StyledGridEntry span={gistTableSpan[1]} key={index++}>
        <StyledRef
          href={newLogEntry.owner.html_url}
          target="_blank"
          rel="noreferrer"
        >
          <StyledImage src={newLogEntry.owner.avatar_url} alt="img" />
        </StyledRef>
      </StyledGridEntry>
      <StyledGridEntry span={gistTableSpan[2]} key={index++}>
        <StyledDispatchButton
          onClick={() => {
            dispatch(setCurrentScreen(Screen.ClickedPublicGist));
            dispatch(setCurrentGist(newLogEntry.id));
          }}
        >
          {newLogEntry.owner.login}
        </StyledDispatchButton>
      </StyledGridEntry>
      <StyledGridEntry span={gistTableSpan[3]} key={index++}>
        {time}
      </StyledGridEntry>
      <StyledGridEntry span={gistTableSpan[4]} key={index++}>
        {date}
      </StyledGridEntry>
      <StyledGridEntry span={gistTableSpan[5]} key={index++}>
        {newLogEntry.files.filename}
      </StyledGridEntry>
      <StyledGridEntry span={gistTableSpan[6]} key={index++}>
        {newLogEntry.files.type}
      </StyledGridEntry>

      <StyledGridEntry span={gistTableSpan[7]} key={index++}>
        <StyledButtonsDiv>
          <div>
            <StarButton id={newLogEntry.id} preview={true} />
          </div>
          <div>
            <ForkButton id={newLogEntry.id} preview={true} />
          </div>
        </StyledButtonsDiv>
      </StyledGridEntry>

      <StyledGridEntry></StyledGridEntry>
    </StyledGrid>
    <hr style={{border:"1px solid rgb(193, 225, 193)"}}></hr>
    </>
  );
}
