import { useSelector } from "react-redux";
import { GistStored } from "../../reducer/reducer";
import { ListItems } from "./list-items";

export function ListFullPage() {
  const newLogEntry = useSelector((state: GistStored) => state.publicGists);

  return (
    <>
        {newLogEntry.map((newLogEntry, index) => (
          <ListItems newLogEntry={newLogEntry} index={index} />
        ))}
    </>
  );
}
