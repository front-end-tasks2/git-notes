import styled from "styled-components";
import { Checkbox, Col, Row } from "antd";

type Props = {
  clicked?: boolean;
  preview?: boolean;
};

export const StyledGrid = styled(Row)`
  margin-left: 10px;
`;

export const StyledGridEntry = styled(Col)`
  text-align: left;
  font-size: 18px;
  word-break: break-all;
  margin: 5px 0 5px 0;
`;

export const StyledHeaderEntry = styled(StyledGridEntry)`
  margin-top: 16px;
  font-weight:500;
  color:black;
`;

export const StyledForkButton = styled.button`
  color: ${(props: Props) => (props.clicked ? "orange" : "#04AA6D")};
  background-color: white;
  border: none;
  cursor: pointer;
  &:hover {
    background-color: lightblue;
  }
`;

export const StyledStarButton = styled.button`
  color: ${(props: Props) => (props.clicked ? "orange" : "#04AA6D")};
  background-color: white;
  border: none;
  cursor: pointer;
  &:hover {
    background-color: lightblue;
  }
`;

export const StyledTable = styled.div``;

export const StyledTableHeading = styled(Row)`
  background-color: #c1e1c1;
  margin: 20px 0 20px 0;
  height: 50px;
`;

export const StyledImage = styled.img`
  width: 20px;
  height: 20px;
  border-radius: 50%;
  margin-right: 10px;
`;

export const StyledBox = styled(Checkbox)`
  width: 15px;
  height: 15px;
  margin-left: 10px;
  color: #04aa6d;
`;

export const StyledWaitingImage = styled.img`
  width: 30px;
  height: 30px;
  margin: 100px;
`;

export const StyledRef = styled.a`
  text-decoration: none;
  color: #04aa6d;
`;

export const StyledDispatchButton = styled.button`
  border: none;
  background-color: white;
  text-align: inherit;
  width: -webkit-fill-available;
  text-align: left;
  font-size: ${(props: Props) => (props.preview ? "12px" : "18px")};
  text-decoration: none;
  color:#504646;
`;

export const StyledButtonsDiv = styled.div`
  display: flex;
  flex-direction: row;
`;
