* react-router-dom library to navigate between pages.
* react hook form
* Switch layouts
* Load multiple pages
* Login page + history of logged in

* Questions
    * Star and fork options??
    * Use of forms??
    * How to link github (for login)
        * https://docs.github.com/en/apps/oauth-apps/building-oauth-apps/authorizing-oauth-apps
    * How to get profiles and data
        * https://docs.github.com/en/rest/gists/gists?apiVersion=2022-11-28#list-public-gists



-----
## State Management

* Store:
    * Login name against password. Auth status (API)
    * User's gists
    * User's access options
    * All API data goes in store
    * Components fetch everything from store
    * New gists dispatched to store
    * Button actions: API calls. Result dispatched to store

* API calls -> fetch, types, REST APIs,
* Antd table
* Items per page (in API call)
* Next button makes API calls (request page number in API)


Event -> API (action) -> Result (store) -> render


Event: btn, page load

* Starting point: 
    * Auth